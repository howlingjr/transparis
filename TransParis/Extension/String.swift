//
//  String.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 14/07/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
