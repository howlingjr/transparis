//
//  Theme.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 08/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

extension Color {
    static let primary = Color(red: 225 / 255, green: 225 / 255, blue: 235 / 255) // white
    static let secondary = Color(red: 129 / 255, green: 128 / 255, blue: 152 / 255) // violet
    static let tertiary = Color(red: 246 / 255, green: 180 / 255, blue: 2 / 255) // yellow
}

extension UIColor {
    static let primary = UIColor(red: 225 / 255, green: 225 / 255, blue: 235 / 255, alpha: 1.0)
    static let secondary = UIColor(red: 129 / 255, green: 128 / 255, blue: 152 / 255, alpha: 1.0)
    static let tertiary = UIColor(red: 246 / 255, green: 180 / 255, blue: 2 / 255, alpha: 1.0)
}

public enum TransparisFont: String {
    case primary = "Avenir"
    case secondary = "AvenirNext-Bold"
    case tertiary = "Helvetica Neue"
}
