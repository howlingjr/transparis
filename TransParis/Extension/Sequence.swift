//
//  Sequence.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        return Dictionary.init(grouping: self, by: key)
    }
}
