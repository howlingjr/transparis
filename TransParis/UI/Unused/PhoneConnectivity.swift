//
//  PhoneConnectivity.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import WatchConnectivity

public class PhoneConnectivity : NSObject,  WCSessionDelegate{
    var session: WCSession
    init(session: WCSession = .default){
        self.session = session
        super.init()
        self.session.delegate = self
        session.activate()
    }
   public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
   public func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    public func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    
    
    
}
