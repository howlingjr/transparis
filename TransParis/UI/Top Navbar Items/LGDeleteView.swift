//
//  LGDeleteView.swift
//  TransParis
//
//  Created by ludovic grimbert on 05/07/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI

public struct DeleteView: View {
    let imageName : String
    
    public var body: some View {
        ZStack {
            
            LGWhiteNeuMorphismView(width: 30, height: 30, style: .circleLowShadow, color: Color.backgroundColor)
            
            HStack {
                Image(imageName)
                    .resizable()
                    .frame(width: 15, height: 15)
                    .foregroundColor(.primaryColor)
                
            }
        }
    }
}
