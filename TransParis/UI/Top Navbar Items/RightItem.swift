//
//  LGDeleteView.swift
//  TransParis
//
//  Created by ludovic grimbert on 05/07/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct RightItem: View {
    
    let width: CGFloat = 30
    let height: CGFloat = 30
    let imageName : String
    
    public var body: some View {
        ZStack {
            LGNeumorphismView(style: .circle, level: .low, vision: .shadow, width: width, height: height, color: Color.primary)
            
            HStack {
                Image(imageName)
                    .resizable()
                    .frame(width: width/2, height: height/2)
                    .foregroundColor(.secondary)
                
            }
        }
    }
}
