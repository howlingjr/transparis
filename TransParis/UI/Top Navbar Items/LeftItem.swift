//
//  BackView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 01/06/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct LeftItem: View {
    
    let width: CGFloat = 80
    let height: CGFloat = 30

    public var body: some View {
        ZStack {
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .low, vision: .shadow, width: width, height: height, color: Color.primary)
            
            HStack {
                Image("back")
                    .renderingMode(.template)
                    .resizable()
                    .frame(width: width/13, height: height/3)
                    .foregroundColor(.secondary)
                
                Text("Retour").foregroundColor(.secondary)
                    .font(.caption)
            }
        }
    }
}
