//
//  SendToWatchView.swift
//  GetCity
//
//  Created by Ludovic Grimbert on 07/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism


/// Apple watch
public struct WatchView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var isToggle : Bool = false
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) { LeftItem() }
    }
    
    public var body: some View {
        ZStack {
            Color.primary
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .high, vision: .shadow, width: 300, height: 300, color: Color.primary)
            
           
            
        }.navigationBarTitle("APPLE WATCH", displayMode: .large)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)
        .edgesIgnoringSafeArea(.all)
    }
}



//
///// Apple watch
//public struct SendToWatchView: View {
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
//    @State var isToggle : Bool = false
//
//    var backButton : some View { Button(action: {
//        self.presentationMode.wrappedValue.dismiss()
//    }) { LeftItem() }
//    }
//
//    public var body: some View {
//        ZStack {
//            Color.primary
//            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .high, vision: .shadow, width: 300, height: 300, color: Color.primary)
//
//            Button(action: {
//                print("Button tapped")
//                self.isToggle = self.isToggle ? false : true
//            }) {
//                Image("cross1")
//                    .renderingMode(.original)
//                    .resizable()
//                    .frame(width: 50, height: 50)
//            }.buttonStyle(SimpleButtonStyle(isToggle: isToggle ))
//
//        }.navigationBarTitle("APPLE WATCH", displayMode: .large)
//        .navigationBarBackButtonHidden(true)
//        .navigationBarItems(leading: backButton)
//        .edgesIgnoringSafeArea(.all)
//    }
//}
//
//struct SimpleButtonStyle: ButtonStyle {
//    @State var isToggle : Bool = false
//
//    func makeBody(configuration: Self.Configuration) -> some View {
//        configuration.label
//            .padding(50)
//            .contentShape(Circle())
//            .background(
//                Group {
//                    if isToggle {
//                        LGNeumorphismView(style: .circle, level: .high, vision: .deep, width: 150, height: 150, color: Color.primary)
//                    } else {
//                        LGNeumorphismView(style: .circle, level: .high, vision: .shadow, width: 150, height: 150, color: Color.primary)
//                    }
//                }
//            )
//    }
//}
