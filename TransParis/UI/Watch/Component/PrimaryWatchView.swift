//
//  PrimaryWatchView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 25/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct PrimaryWatchView: View {
    let width: CGFloat
    let height: CGFloat
    
    public var body: some View {
        ZStack{
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25),
                              level: .high,
                              vision: .shadow,
                              width: width/1.5,
                              height: height/1.5,
                              color: .primary)
            
        }
    }
}
