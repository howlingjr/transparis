//
//  SearchStationCellView.swift
//  TransParis
//
//  Created by ludovic grimbert on 02/07/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism
import CustomFont

public struct SearchStationCellView: View {
    let station : Station
   // var action: () -> Void
    
    public var body: some View {
//        Button(action: {
//            self.action()
//        }) {
            ZStack {
                LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .low, vision: .shadow, width: 300, height: 70, color: Color.primary)
                
                HStack {
                    
                    Text(station.name)
                        .lineLimit(nil)
                        .frame(minWidth: 0, idealWidth: 120, maxWidth: 180, minHeight: 0, idealHeight: nil, maxHeight: nil, alignment: .center)
                    .foregroundColor(Color.secondary)
                        .customFont(name: TransparisFont.tertiary.rawValue, style: .title3, weight: .bold)
                    
                    Text(station.renameTypeTransport())
                        .frame(width: 40, height: 26, alignment: .center)
                        .foregroundColor(Color.primary)
                        .customFont(name: TransparisFont.tertiary.rawValue, style: .caption1, weight: .bold)
                        .background(Circle()
                            .fill(Color.secondary)).opacity(1.0)
                    
                    Text(station.code)
                        .frame(width: 35, height: 35, alignment: .center)
                        .foregroundColor(Color.secondary)
                        .customFont(name: TransparisFont.tertiary.rawValue, style: .body, weight: .bold)
                        .overlay(
                            Circle()
                                .stroke(Color.secondary, lineWidth: 1))
                    
                }
            } .frame(width: 300, height: 80,alignment: .leading)
            
      //  }
    }
}
