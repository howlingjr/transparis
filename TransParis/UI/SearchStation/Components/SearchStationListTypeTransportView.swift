//
//  SearchStationListTypeTransportView.swift
//  TransParis
//
//  Created by ludovic grimbert on 07/07/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

struct SearchStationListTypeTransportView: View {
    var viewModel: SearchStationViewModel
    @State private var selection: Set<TypeTransport.RawValue> =  []
    
    var body: some View {
        ZStack {
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .low, vision: .shadow, width: 300, height: 100, color: Color.primary)
            
            scrollForEach.frame(width: 250, height: 100)
        }
    }
    
    var scrollForEach: some View {
        
        ScrollView(.horizontal, content: {
            HStack(spacing: 5) {
                ForEach(TypeTransport.list,id:\.self) { type in
                    
                    SearchStationTypeTransportView(type: type, holed: self.selection.contains(type))
                        .onTapGesture {
                            self.selectDeselect(type)
                    }
                }
            }
        })
    }
    
    /// Select cell
    private func selectDeselect(_ type: TypeTransport.RawValue) {
        if selection.contains(type) {
            selection.remove(type)
            self.viewModel.removeStations(type: type)
        } else {
            selection.insert(type)
            self.viewModel.addStations(type: type)
        }
    }
}
