//
//  SearchStationTypeTransportView.swift
//  TransParis
//
//  Created by ludovic grimbert on 06/07/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct SearchStationTypeTransportView: View {
    let type : String
    let holed : Bool
    
    public var body: some View {
        
        VStack {
            ZStack {
    
                LGNeumorphismView(style: .circle, level: .high, vision: self.holed ? .deep : .shadow, width: 45, height: 45, color: Color.clear)
                
                Image("\(type)")
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 21, height: 26)
            }
            
            Text("\(translateShortly(type: type))")
                .customFont(name: TransparisFont.tertiary.rawValue, style: .caption1, weight:self.holed ? .bold : .light)
                .foregroundColor(Color.secondary)
                .frame(width: 21, height: 10, alignment: .center)
            
        }
    }
    
    func translateShortly(type: String) -> String {
        
        switch type {
        case TypeTransport.metros.rawValue: return TypeTransport.metros.description
        case TypeTransport.buses.rawValue: return TypeTransport.buses.description
        case TypeTransport.rers.rawValue: return TypeTransport.rers.description
        case TypeTransport.noctiliens.rawValue: return TypeTransport.noctiliens.description
        case TypeTransport.tramways.rawValue: return TypeTransport.tramways.description
            
        default:
            return "none"
        }
    }
}
