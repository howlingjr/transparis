//
//  AddStationView.swift
//  GetCity
//
//  Created by Ludovic Grimbert on 07/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

/// Add Station
public struct SearchStationView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) var managedObjectContext
    @ObservedObject var viewModel: SearchStationViewModel = SearchStationViewModel()

    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) { LeftItem() }
    }
    
    public var body: some View {
        
        ZStack {
            Color.primary.edgesIgnoringSafeArea(.all)
            
            VStack {
                Spacer(minLength: 30)
                ZStack {
                    LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .high, vision: .shadow, width: 300, height: 60, color: .primary)
                    
                    HStack(spacing: 20) {
                        
                        Image("search")
                            .renderingMode(.original)
                            .resizable()
                            .frame(width: 30, height: 30, alignment: .center)
                            .padding(.leading, 10)
                        
                        SearchTextField(placeholder: Text("Station (3 caractères minimum)")
                                            .foregroundColor(.secondary),
                                        text: $viewModel.searchText)
                            .frame(width: 200, height: 60, alignment: .center)
                            .textFieldStyle(SearchTextFieldStyle())
                        
                        Button(action: {
                            self.viewModel.searchText = ""
                        }) {
                            Image("delete")
                                .renderingMode(.original)
                                .resizable()
                                .frame(width: 20, height: 20, alignment: .center)
                                .opacity(0.5)
                        } .padding(.leading, -20)
                    }
                }.padding(.bottom, 30)
                
                //TODO: - animation pick pour indiquer qu'il faut cliquer
                /// List type with images
                SearchStationListTypeTransportView(viewModel: viewModel)
                
                Spacer(minLength: 40)
                
                TertiaryHomeView(title: "RESULTATS")
                ScrollView{
                    LazyVStack {
                        ForEach(self.viewModel.stations) { station in
                            NavigationLink(destination: LazyView(ScheduleView(station: station, isEnableToDelete: false))) { SearchStationCellView(station: station) }
                        }
                    }
                }
            } .padding(.bottom, 30)
        }.onAppear {
            print("🟢ContentView appeared!")
            self.viewModel.searchText = ""
            self.viewModel.stations = []
            self.viewModel.stationsTemp = []
            
        }.onDisappear {
            print("🟢ContentView disappeared!")
        }
        .navigationBarTitle("RECHERCHER", displayMode: .large)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct SearchTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        VStack {
            configuration
                .accentColor(.secondary)
                .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .bold)
                .colorMultiply(.white)
                .foregroundColor(.secondary)
        }
    }
}

struct SearchTextField: View {
    var placeholder: Text
    @Binding var text: String
    var editingChanged: (Bool) -> Void = { _ in }
    var commit: () -> Void = { }
    
    var body: some View {
        ZStack(alignment: .leading) {
            if text.isEmpty { placeholder.customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .bold) }
            TextField("", text: $text, onEditingChanged: editingChanged, onCommit: commit).customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .bold)
                .disableAutocorrection(true)
                .keyboardType(.default)
        }
    }
}

//********************************************************************************************

struct SearchStationView_Previews: PreviewProvider {
    static var previews: some View {
        SearchStationView().environment(\.colorScheme, .light)
    }
}

// IMPORTANT !
struct LazyView<Content: View>: View {
    let build: () -> Content
    
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    
    var body: Content {
        build()
    }
}
