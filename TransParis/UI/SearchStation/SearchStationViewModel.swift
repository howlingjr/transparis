//
//  SearchStationViewModel.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 23/06/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import Combine

public class SearchStationViewModel: ObservableObject {
    @Published var stations = [Station]()
    var stationsTemp = [Station]()
    var allStations = [Station]()
    private var finalCancellableStations: AnyCancellable?
    @Published var searchText = "" {
        didSet {
            self.getStationsWith(search: self.searchText)
        }
        willSet {
            self.getStationsWith(search: self.searchText)
        }
    }
    
    init() {
        self.getAllStationsFromJson()
    }
    
    func getStationsFromJson(typeString: String) -> AnyPublisher< [Station], Error> {
        let url = Bundle.main.url(forResource: typeString, withExtension: "json") ?? URL(fileURLWithPath: "")
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: StationRawResponse.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .map { $0.result.stations}
            .eraseToAnyPublisher()
    }
    
    func getAllStationsFromJson() {
        let typesString: [String] = TypeTransport.list
        
        self.finalCancellableStations = Publishers.Sequence(sequence: typesString.map {self.getStationsFromJson(typeString: $0) })
            .flatMap(maxPublishers: .max(5)) { $0 }
            //.flatMap { $0 }
            .receive(on: DispatchQueue.main)
            .collect()
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { stations in
                
                self.allStations = stations.flatMap { $0 }
            })
    }
    
    func getStationsWith(search: String) {
        if search.count >= 3 {
            self.stations = self.stationsTemp.filter {$0.name.lowercased().contains(search.lowercased())}
        } else {
            self.stations = []
        }
    }
    
    func addStations(type : TypeTransport.RawValue) {
        self.stationsTemp += self.allStations.filter {$0.type == type}
        getStationsWith(search: searchText)
    }
    
    func removeStations(type : TypeTransport.RawValue) {
        self.stationsTemp = self.stations.filter {$0.type != type}
        getStationsWith(search: searchText)
    }
}
