//
//  ScheduleView.swift
//  GetCity
//
//  Created by Ludovic Grimbert on 07/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import LGAlert

public struct ScheduleView: View {
    
    @State var currentTimePublisher = Timer.TimerPublisher(interval: 30.0, runLoop: .main, mode: .common).autoconnect() // 30 secondes
    let station: Station
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) var managedObjectContext
    @ObservedObject var viewModel: ScheduleViewModel = ScheduleViewModel()
    let isEnableToDelete : Bool
    @State private var showAlert = false
    @FetchRequest(fetchRequest: RecordingStation.getRecordingStationsFetchRequest()) var recordingStations: FetchedResults<RecordingStation>

    /// Back button
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
        LeftItem()
    }
    }
    
    /// Delete  button
    var deleteStationButton : some View { Button(action: {
        self.showAlert.toggle()
    }) {
        RightItem(imageName: isEnableToDelete ? "garbage":"cross")
    }.LGalert(isPresented: $showAlert,
              content: { LGAlert(title: Text(self.isEnableToDelete ? "Etes-vous sûr de vouloir supprimer cette station ?" : "Etes-vous sûr de vouloir ajouter cette station ?"),
                                 imageName: self.isEnableToDelete ? "garbage" : "cross",
                                 buttonStack: [LGAlert.Button.default(Text("Annuler"),
                                                                      width: 100,
                                                                      height: 50,
                                                                      isNeumorphism: true,
                                                                      fontName: TransparisFont.tertiary.rawValue,
                                                                      style: .body,
                                                                      weight: .light,
                                                                      color: .primary),
                                               LGAlert.Button.default(width: 100,
                                                                      height: 50,
                                                                      isNeumorphism: true,
                                                                      color: .primary,
                                                                      action: { self.isEnableToDelete ? self.deleteStation() : self.addStation()})],
                                 theme: .custom(windowColor: Color.primary,
                                                alertTextColor: Color.secondary,
                                                defaultButtonTextColor: Color.secondary),
                                 animation: .fadeEffect())
              })
    }
    
    public var body: some View {
        
        VStack {
            HStack {
                
                /// Title with activity indicator
                ScheduleTitleView(station: station, viewModel: self.viewModel, action: { self.viewModel.requestSchedules(station: self.station) })
                
            }.padding(.bottom, 10)
            
            /// Expanding cell
            /// Get response from request
            ScheduleCustomEmptyView(isDisplaying: self.viewModel.isEmptySchedule)
            ScheduleListVoView(scheduleGroupingVo: self.viewModel.scheduleGroupingVo)
            
        }.navigationBarTitle("HORAIRES", displayMode: .large)
        .navigationBarColor(UIColor.primary)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton,trailing: deleteStationButton )
        .background(Color.primary)
        .edgesIgnoringSafeArea(.bottom)
        
        /// Fetch data + Timer
        .onAppear {
            self.instantiateTimer()
            /// Launch initial request
            self.viewModel.requestSchedules(station: self.station)
        }.onDisappear {
            /// Delete Timer
            self.currentTimePublisher.upstream.connect().cancel()
        }.onReceive(currentTimePublisher) { newCurrentTime in
            print(newCurrentTime)
            /// Launch request after x seconds
            self.viewModel.requestSchedules(station: self.station)
        }
    }
    
    func instantiateTimer() {
        print("INTERVAL 30 SECONDES ⏰")
        self.currentTimePublisher = Timer.TimerPublisher(interval: 30.0, runLoop: .main, mode: .common).autoconnect()
    }
    
    func deleteStation() {
        guard let index = self.recordingStations.firstIndex(where: {$0.id == self.station.id}) else {return}
        self.managedObjectContext.delete(self.recordingStations[index])
        
        do {
            try self.managedObjectContext.save()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.presentationMode.wrappedValue.dismiss()
            })
        } catch {
            print(error)
        }
    }
    
    func addStation() {
        
        if self.recordingStations.contains(where: {$0.id == station.id}) {
            print("Also exists from recording stations")
        } else {
            let newStationSaved = RecordingStation(context: self.managedObjectContext)
            newStationSaved.allAtributes = station
            
            do {
                try self.managedObjectContext.save()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.presentationMode.wrappedValue.dismiss()
                })
            } catch {
                print(error)
            }
        }
    }
}
