//
//  ScheduleViewModel.swift
//  GetCity
//
//  Created by Ludovic Grimbert on 08/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import Combine

public class ScheduleViewModel: ObservableObject {
    @Published var scheduleGroupingVo: [ScheduleGroupingVo] = [ScheduleGroupingVo]()
    var schedulesVo = [ScheduleVo]()
    @Published var loading = false
    @Published var isEmptySchedule = false
    
    private var cancellable: AnyCancellable?
    
    func findUrlWith(way: Way,station: Station) -> URL? {
        return  URL(string: APIRouter.schedules(AR: way.rawValue, stationSlug: station.slug, code: station.code, type: station.type, baseURL: Constant.Domain.CurrentBaseURL).path)
    }
    
    func requestSchedules(station: Station) {
        self.loading = true
        
        guard let wayA = findUrlWith(way: .directionA, station: station) else {return}
        guard let wayR = findUrlWith(way: .directionR, station: station) else {return}
        
        /// Schedules A
        let publisherWayA = URLSession.shared.dataTaskPublisher(for: wayA)
            .receive(on: DispatchQueue.main)
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: RawResponse.self, decoder: JSONDecoder())
        
        /// Schedules R
        let publisherWayR = URLSession.shared.dataTaskPublisher(for: wayR)
            .receive(on: DispatchQueue.main)
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: RawResponse.self, decoder: JSONDecoder())
        
        /// Merge Schedules
        self.cancellable = Publishers.Zip(publisherWayA, publisherWayR)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    // fatalError(error.localizedDescription) // CRASH
                    print(error)
                // TODO:- faire une alerte
                }
            }, receiveValue: { (rawReponseA,rawReponseR) in
                print("REQUEST SCHEDULES")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    /// Convert schedule to scheduleVo with way
                    self.schedulesVo = [ScheduleVo]()
                    var newVo = ScheduleVo()
                    
                    /// add way A
                    for element in rawReponseA.result.schedules {
                        newVo = ScheduleVo(id: element.id, message: self.fixMessage(element.message), destination: element.destination, code: element.code ?? "", way: .directionA)
                        self.schedulesVo.append(newVo)
                    }
                    
                    /// add way R
                    for element in rawReponseR.result.schedules {
                        newVo = ScheduleVo(id: element.id, message: self.fixMessage(element.message), destination: element.destination, code: element.code ?? "", way: .directionR)
                        self.schedulesVo.append(newVo)
                    }
                    
                    /// Remove some unused messages
                    self.schedulesVo = self.schedulesVo.filter {!UnusedMessageRATP.messagesList.contains(where: $0.message.lowercased().contains)}
                    
                    /// Final Schedules
                    self.scheduleGroupingVo = self.convertToScheduleGroupingVo(schedulesVo: self.schedulesVo)
                    
                    /// Stop loader
                    self.loading = false
                    
                    /// Display custom empty view
                    if self.scheduleGroupingVo.isEmpty {
                        self.isEmptySchedule = true
                    } else {
                        self.isEmptySchedule = false
                    }
                    
                }
            })
    }
    
    fileprivate func fixMessage(_ message: String) -> String {
        switch message.lowercased() {
        case GoodMessageRATP.trainApproche.rawValue: return GoodMessageRATP.trainApprocheFix.rawValue.capitalizingFirstLetter()
        case GoodMessageRATP.trainAQuai.rawValue : return GoodMessageRATP.trainAQuaiFix.rawValue.capitalizingFirstLetter()
        case GoodMessageRATP.approche.rawValue : return GoodMessageRATP.approcheFix.rawValue.capitalizingFirstLetter()
        case GoodMessageRATP.aLarret.rawValue : return GoodMessageRATP.aLarretFix.rawValue.capitalizingFirstLetter()
        default:
            return message
        }
    }
    
    fileprivate func convertToScheduleGroupingVo(schedulesVo: [ScheduleVo]) -> [ScheduleGroupingVo] {
        var closingSchedules : [ScheduleGroupingVo] = []
        
        /// Grouping by Destination
        let grouped = schedulesVo.group(by: { $0.destination })
        
        /// Set closed schedules
        for element in grouped {
            if GoodMessageRATP.messagesList.filter({ $0 == element.value.first?.message.lowercased() }).first != nil {
                closingSchedules.append(ScheduleGroupingVo(id: UUID(), destination: element.key, way: element.value.first?.way ?? .none, result: Array(element.value), isClosed: true))
            } else {
                closingSchedules.append(ScheduleGroupingVo(id: UUID(), destination: element.key, way: element.value.first?.way ?? .none, result: Array(element.value), isClosed: false))
            }
        }
        
        var bestSchedules : [ScheduleGroupingVo] = []
        
        for element in GoodMessageRATP.messagesList {
            let schedules = closingSchedules.filter({element.contains(($0.result.first?.message.lowercased()) ?? "") })
            bestSchedules.append(contentsOf: schedules)
        }
        print("best schedules count: \(bestSchedules.count)")
        
        var otherSchedules : [ScheduleGroupingVo] = []
        otherSchedules = closingSchedules.filter({!GoodMessageRATP.messagesList.contains($0.result.first?.message.lowercased() ?? "") })
        
        guard let message = otherSchedules.first?.result.first?.message else {
            print("Not message from otherSchedules")
            return bestSchedules.removingDuplicates()
        }
        
        if message.contains(":") {
            print("schedules RER")
            otherSchedules = closingSchedules.sorted(by: { convertStringToSpecialDate(message: $0.result.first?.message ?? "00:00") <= convertStringToSpecialDate(message: $1.result.first?.message ?? "00:00") })
        } else {
            print("schedules NOT RER")
            otherSchedules = closingSchedules.sorted(by: { removeMnString(string: $0.result.first?.message ?? "0 mn") <= removeMnString(string: $1.result.first?.message ?? "0 mn") })
        }
        print("other schedulescount : \(otherSchedules.count)")
        
        return (bestSchedules + otherSchedules).removingDuplicates()
    }
    
    func removeMnString(string: String) -> Int {
        return Int(string.replacingOccurrences(of: " mn", with: "")) ?? 0
    }
    
    func convertStringToSpecialDate(message: String) -> Date {
        guard let hour = Int(message.dropLast(3)) else {return Date()}
        guard let minute = Int(message.dropFirst(3)) else {return Date()}
        
        var components = DateComponents()
        switch hour {
        case 0...4: components.day = 1
        default: components.day = 0
        }
        components.hour = hour
        components.minute = minute
        let date = Calendar.current.date(from: components) ?? Date()
        //print("date🔵 : \(date)")
        return date
    }
    
}


