//
//  ScheduleCustomEmptyView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 13/07/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism
import LGNeumorphism

public struct ScheduleCustomEmptyView: View {
    let isDisplaying : Bool
    @State private var isAnimating = false
    
    var foreverAnimation: Animation {
        Animation.linear(duration: 5.0)
            .repeatForever(autoreverses: true)
    }
    
    public var body: some View {
        
        content
        
    }
    
    private var content: some View {
        
        ZStack {
            if isDisplaying {
                
                ZStack {
                    
                    LGNeumorphismView(style: .triangle, level: .high, vision: .shadow, width: 230, height: 230, color: Color.primary)
                    LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .low, vision: .deep, width: 30, height: 50, color: Color.primary)
                    LGNeumorphismView(style: .circle, level: .high, vision: .deep, width: 20, height: 20, color: Color.primary).padding(.top, 90)

                                    
                    Text("pas de service visible").frame(width: 230, height: nil, alignment: .center)
                        .foregroundColor(.secondary)
                        .font(.caption)
                        .padding(.top, 180)
                }
                .offset(x: 0.0, y: self.isAnimating ? 0 : 200)
                .animation(self.isAnimating ? foreverAnimation : .none)
                .onAppear { self.isAnimating = true }
                .onDisappear { self.isAnimating = false }
                
            } else {
                EmptyView()
            }
        }
    }
}
