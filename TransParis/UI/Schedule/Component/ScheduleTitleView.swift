//
//  ScheduleTitleView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGActivityIndicator
import LGNeumorphism


public struct ScheduleTitleView: View {
    let station : Station
    @ObservedObject var viewModel: ScheduleViewModel
    var action: () -> Void
    
    public var body: some View {
        
        HStack {
            VStack {
                Text(station.renameTypeTransport() + " " + station.code)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 20, alignment: .leading)
                    .foregroundColor(Color.secondary)
                    .customFont(name: TransparisFont.tertiary.rawValue, style: .title1, weight: .light)
                    .padding(.leading, 16)
                
                Text(station.name.capitalized)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 100, alignment: .leading)
                    .foregroundColor(Color.secondary)
                    .customFont(name: TransparisFont.tertiary.rawValue, style: .title1, weight: .light)
                    .padding(.leading, 16)
            }
            
            Button(action: {
                print("Call Schedules by push button ")
                self.action()
            }) {
                
                ZStack {
                    LGNeumorphismView(style: .circle, level: .low, vision: .shadow, width: 50, height: 50, color: Color.primary).padding(.trailing, 10)
                    
                    HStack(spacing: 3) {
                        
                        LGNeumorphismView(style: .circle, level: .high, vision: .deep, width: 8, height: 8, color: Color.primary)
                        LGNeumorphismView(style: .circle, level: .high, vision: .deep, width: 8, height: 8, color: Color.primary)
                        LGNeumorphismView(style: .circle, level: .high, vision: .deep, width: 8, height: 8, color: Color.primary)
                    }.padding(.trailing, 10)
                    
                    LGActivityIndicatorView(isVisible: $viewModel.loading, type: .ghostDots(count: 3))
                        .frame(width: 30, height: 10,alignment: .trailing)
                        .padding(.trailing, 10)
                        .foregroundColor(Color.white)
                }
                
            }
        }
    }
}
