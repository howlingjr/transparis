//
//  ScheduleVoView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGActivityIndicator
import LGNeumorphism

struct ScheduleCellVoView: View {
    
    
    let scheduleGroupingVo: ScheduleGroupingVo
    let isExpanded: Bool
    @State var loading = true
    
    var body: some View {
        HStack {
            content
        }
        .contentShape(Rectangle())
    }
    
    private var content: some View {
        
        VStack(alignment: .leading) {
            HStack {
                
                /// First schedule ( color way + direction )
                
                ZStack {
                    LGNeumorphismView(style: .circle, level: .high, vision: .deep, width: 20, height: 20, color: scheduleGroupingVo.isClosed ? Color.tertiary : Color.primary).padding(.trailing, 10)
                    LGNeumorphismView(style: .circle, level: .low, vision: .shadow, width: 8, height: 8, color: Color.primary).padding(.trailing, 10)
                    
                    if scheduleGroupingVo.isClosed {
                        LGActivityIndicatorView(isVisible: $loading, type: .circleDots(count: 5))
                            .frame(width: 19, height: 19)
                            .padding(.trailing, 10)
                            .foregroundColor(Color.primary)
                    }
                }
                
                //                Text(scheduleGroupingVo.way == .A ? "⇲" : "⇱")
                //                                   .frame(width: 20, height: 20, alignment: .center)
                //                                   .foregroundColor(Color.backgroundColor)
                //                                   .customFont(name: "Helvetica Neue", style: .title2, weight: .bold)
                //                                   .background(Rectangle()
                //                                     .fill(Color.primaryColor)).opacity(1.0)
                
                Text(scheduleGroupingVo.destination)
                    .customFont(name: TransparisFont.primary.rawValue, style: .title2, weight: .light)
                    .foregroundColor(Color.secondary)
            }
            
            HStack {
                
                Text("\(scheduleGroupingVo.result.first!.message)")
                    .customFont(name: TransparisFont.primary.rawValue, style: .title3, weight: .bold)
                    .foregroundColor(Color.secondary)
                
                Text("\(scheduleGroupingVo.result.first!.code)")
                    .customFont(name: TransparisFont.primary.rawValue, style: .title3, weight: .bold)
                    .foregroundColor(Color.secondary)
                
                Spacer(minLength: 20)
                
                /// Display chevron when there are multi schedules
                if scheduleGroupingVo.result.count > 1 {
                    ZStack {
                        
                        LGNeumorphismView(style: .circle, level: .high, vision: isExpanded ? .deep : .shadow, width: 30, height: 30, color: Color.primary)
                            .padding(.trailing, 30)
                            .animation(.spring())
                        
                        Image("chevron-dark")
                            .resizable()
                            .frame(width: 15, height: 15, alignment: .trailing)
                            .foregroundColor(Color.secondary)
                            .accentColor(.orange)
                            .rotationEffect(.degrees(isExpanded ? 90 : -90))
                            .padding(.trailing, 30)
                            .animation(.spring())
                    }
                    
                }
            }
            
            /// Expand when there are multi schedules ( else disable cell )
            if self.scheduleGroupingVo.result.count > 1 {
                ForEach(Array(zip(1..., scheduleGroupingVo.result)), id: \.1.id) { number, element in
                    VStack(alignment: .leading) {
                        if number != 1 {
                            HStack {
                                Text(self.isExpanded ? element.message : "")
                                    .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .light)
                                    .foregroundColor(Color.secondary)
                                    .animation(.spring())

                                Text(self.isExpanded ? element.code : "")
                                    .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .light)
                                    .foregroundColor(Color.secondary)
                                    .animation(.spring())

                            }.frame(minWidth: 0, idealWidth: nil, maxWidth: .infinity, minHeight: 0, idealHeight: nil, maxHeight: self.isExpanded ? 200 : 0, alignment: .leading)
                        }
                    }
                }
            }
            
//            if self.isExpanded && self.scheduleGroupingVo.result.count > 1 {
//                ForEach(Array(zip(1..., scheduleGroupingVo.result)), id: \.1.id) { number, element in
//                    VStack(alignment: .leading) {
//                        if number != 1 {
//                            HStack {
//                                Text(element.message)
//                                    .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .light)
//                                    .foregroundColor(Color.secondary)
//                                   // .animation(.spring())
//
//                                Text(element.code)
//                                    .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .light)
//                                    .foregroundColor(Color.secondary)
//                                   // .animation(.spring())
//                            }
//                        }
//                    }
//                }
//            }
        }
    }
}
