//
//  SchedulesVoListView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI

struct ScheduleListVoView: View {
    
    let scheduleGroupingVo: [ScheduleGroupingVo]
    @State private var selection: Set<ScheduleGroupingVo> = []
    
    var body: some View {
        scrollForEach
    }
    
    var scrollForEach: some View {
        ScrollView {
           LazyVStack {
            ForEach(scheduleGroupingVo) { scheduleGroupingVo in
                ScheduleCellVoView(scheduleGroupingVo: scheduleGroupingVo, isExpanded: self.selection.contains(scheduleGroupingVo))
                    .modifier(ListRowModifier())
                    .onTapGesture { self.selectDeselect(scheduleGroupingVo)}
                    //.animation(.linear(duration: 0.3))
            }
        }
        }
    }
    
    /// Select cell
    private func selectDeselect(_ scheduleGroupingVo: ScheduleGroupingVo) {
        if selection.contains(scheduleGroupingVo) {
            selection.remove(scheduleGroupingVo)

        } else {
            selection.insert(scheduleGroupingVo)
        }
    }
}

struct ListRowModifier: ViewModifier {
    func body(content: Content) -> some View {
        Group {
            content
           // Divider()
            Spacer(minLength: 50)
        }.offset(x: 20)
    }
}

//**********************************************************************************************

//
//struct SchedulesVoList_Previews: PreviewProvider {
//    static var previews: some View {
//        //SchedulesVoListView(schedulesVo:self.viewModel.schedulesVo)
//    }
//}
