//
//  FetchStationViewModel.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 23/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import Combine

public struct CustomUrl {
    let urls: URL
    let code: String
}

public class JsonStationViewModel: ObservableObject {
    @Published var stationCount: Int = 0

    var lines = Lines()
    private var cancellableLines: AnyCancellable?
    private var cancellableStations: AnyCancellable?
    
    /// Set true to make JSON with console
    var isPrinting: Bool = false
    
    func url(_ type: TypeTransport) -> URL {
        let url = URL(string: APIRouter.lines(type: type.rawValue,
                                              baseURL: Constant.Domain.CurrentBaseURL).path) ?? URL(fileURLWithPath: "")
        return  url
    }
    
    func url(_ type: TypeTransport, code: String) -> URL? {
        return  URL(string: APIRouter.stations(type: type.rawValue,
                                               code: code,
                                               baseURL: Constant.Domain.CurrentBaseURL).path)
    }
    
    func getStations(type: TypeTransport) {
        switch type {
        case .metros: self.getMetroStations()
        case .buses: self.getBusStations()
        case .rers: self.getRerStations()
        case .noctiliens: self.getNoctilienStations()
        case .tramways: self.getTramwayStations()
        }
    }
    
    func getMetroStations() {
         lines = Lines()
        let publisher = URLSession.shared.dataTaskPublisher(for: url(.metros))
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: MetroRawResponse.self, decoder: JSONDecoder())
            .map { $0.result.metros}
        
        self.cancellableLines = publisher
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { rawResponse in
                let filtred = rawResponse.filter {!$0.code.contains {$0.isLetter}}
                for element in filtred {
                    self.lines.append(element)
                }
                self.mergeAllStations(type: .metros)
            })
    }
    
    func getBusStations() {
        lines = Lines()
        let publisher = URLSession.shared.dataTaskPublisher(for: url(.buses))
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: BusRawResponse.self, decoder: JSONDecoder())
            .map { $0.result.buses}
        
        self.cancellableLines = publisher
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { rawResponse in
                /// BUS without Orlybus and ey-Fla and Hironde and Pc
                let filtred = rawResponse.filter {$0.code != "Orlybus" && $0.code != "ey-Fla" && $0.code != "Hironde" && $0.code != "Pc"}
                for element in filtred {
                    self.lines.append(element)
                }
                self.mergeAllStations(type: .buses)
            })
    }
    
    func getRerStations() {
        lines = Lines()
        let publisher = URLSession.shared.dataTaskPublisher(for: url(.rers))
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: RerRawResponse.self, decoder: JSONDecoder())
            .map { $0.result.rers}
        
        self.cancellableLines = publisher
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { rawResponse in
                /// RER without C and D
                let filtred = rawResponse.filter {$0.code == "A" || $0.code == "B"}
                for element in filtred {
                    self.lines.append(element)
                }
                self.mergeAllStations(type: .rers)
            })
    }
    
    func getNoctilienStations() {
        lines = Lines()
        let publisher = URLSession.shared.dataTaskPublisher(for: url(.noctiliens))
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: NoctilienRawResponse.self, decoder: JSONDecoder())
            .map { $0.result.noctiliens}
        
        self.cancellableLines = publisher
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { rawResponse in
                let filtred = rawResponse.filter {!$0.code.contains {$0.isLetter}}
                for element in filtred {
                    self.lines.append(element)
                }
                self.mergeAllStations(type: .noctiliens)
            })
    }
    
    func getTramwayStations() {
        lines = Lines()
        let publisher = URLSession.shared.dataTaskPublisher(for: url(.tramways))
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: TramwayRawResponse.self, decoder: JSONDecoder())
            .map { $0.result.tramways}
        
        self.cancellableLines = publisher
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { rawResponse in
                let filtred = rawResponse.filter {!$0.code.contains {$0.isLetter}}
                for element in filtred {
                    self.lines.append(element)
                }
                self.mergeAllStations(type: .tramways)
            })
    }
    
    func getStations(from url: URL, code: String, type: String) -> AnyPublisher< [Station], Error> {
        var stations = [Station]()
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: RawStationRawResponse.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .map {$0.result.stations }
            .tryMap { output in
                for element in output {
                    let identifier = UUID()
                    stations.append(Station(id: identifier.uuidString,
                                            name: element.name,
                                            type: type,
                                            code: code,
                                            imageName: type,
                                            slug: element.slug))
                }
                return stations
            }
            .eraseToAnyPublisher()
    }
    
    func mergeAllStations(type: TypeTransport) {
        var customUrls = [CustomUrl]()
        
        /// 120 Max - Bus warning
        for index in 0...self.lines.count-1 {
            if let urls = self.url(type, code: self.lines[index].code) {
                customUrls.append(CustomUrl(urls: urls, code: self.lines[index].code))
            }
        }
        
        self.cancellableStations = Publishers.Sequence(sequence: customUrls.map { self.getStations(from: $0.urls,
                                                                                                           code: $0.code,
                                                                                                           type: type.rawValue) })
            .flatMap(maxPublishers: .max(1)) { $0 }
            //.flatMap { $0 }
            .collect()
            .eraseToAnyPublisher()
            
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { stations in
                let flattenedStations = stations.flatMap { $0 }
                
                print("Flattened stations count: \(flattenedStations.count)")
                let uniqueStations = Array(Set(flattenedStations))
                print("Unique stations count: \(uniqueStations.count)")
                self.stationCount = uniqueStations.count

                self.createJson(with: uniqueStations)
            })
    }
    
    func createJson(with stations: [Station]) {
        var prodArray : [[String:Any]] = [[String:Any]]()
        
        for element in stations {
            var station : [String:Any] = [String:Any]()
            station["id"] = element.id
            station["name"] = element.name
            station["slug"] = element.slug
            station["type"] = element.type
            station["code"] = element.code
            station["imageName"] = element.imageName
            
            prodArray.append(station)
        }
        
        let stations = ["stations": prodArray]
        let result = ["result": stations]
        let string = String(data: try! JSONSerialization.data(withJSONObject: result, options: .prettyPrinted), encoding: .utf8)!
        
        /// Get the Log to make a JSON !!!!!!!!
        guard isPrinting else {return}
        print(string)
    }
}
