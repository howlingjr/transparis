//
//  FetchStationView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 23/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct JsonStationView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var viewModel: JsonStationViewModel = JsonStationViewModel()
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) { LeftItem() }
    }
    
    public var body: some View {
        GeometryReader { geometry in
            ZStack {
                Color.primary
                VStack(spacing: geometry.size.width/10) {
                    PrimaryJsonStationView(viewModel: viewModel,
                                           width: geometry.size.width,
                                           height: geometry.size.width)
                    TertiaryJsonStationView(stationCount: viewModel.stationCount,
                                            width: geometry.size.width,
                                            height: geometry.size.width)
                }
            }.navigationBarTitle("PREPARER JSON", displayMode: .large)
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: backButton)
            .edgesIgnoringSafeArea(.all)
        }
    }
}

