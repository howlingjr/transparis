//
//  PrimaryJsonStationView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 24/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct PrimaryJsonStationView: View {
    @State var viewModel: JsonStationViewModel
    let width: CGFloat
    let height: CGFloat
    
    public var body: some View {
        ZStack{
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25),
                              level: .high,
                              vision: .shadow,
                              width: width/1.5,
                              height: height/1.5,
                              color: .primary)
            LazyVStack() {
                ForEach(TypeTransport.cases, id: \.self) { type in
                    SecondaryJsonStationView(type: "\(type)",
                                             width: width,
                                             height: height)
                        .onTapGesture {
                            self.viewModel.getStations(type: type)
                            print("create json")
                        }
                }
            }
        }
    }
}

