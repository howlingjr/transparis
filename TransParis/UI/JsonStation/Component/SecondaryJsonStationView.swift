//
//  SecondaryJsonStationView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 24/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI

public struct SecondaryJsonStationView: View {
    let type : String
    let width: CGFloat
    let height: CGFloat
    
    public var body: some View {
        VStack {
            Text(type)
                .frame(width: width/2, height: height/12, alignment: .center)
                .font(.caption)
                .foregroundColor(Color.primary)
                .background(Color.secondary)
            Spacer(minLength: 10)
        }
    }
}
