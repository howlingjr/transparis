//
//  TertiaryJsonStationView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 24/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct TertiaryJsonStationView: View {
    let stationCount: Int
    let width: CGFloat
    let height: CGFloat
    
    public var body: some View {
        ZStack{
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25),
                              level: .low,
                              vision: .shadow,
                              width: width/1.5,
                              height: height/12,
                              color: Color.primary)
            
            Text("Il y a \(stationCount) stations")
                .frame(width: width/1.7, height: height/13, alignment: .center)
                .font(.caption)
                .foregroundColor(Color.secondary)
                .background(Color.primary)
        }
    }
}
