//
//  HomeView.swift
//
//  Created by ludovic grimbert on 15/04/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
//    @FetchRequest(
//        entity: RecordingStation.entity(),
//        sortDescriptors: [ NSSortDescriptor(keyPath: \RecordingStation.name, ascending: true)]
//    ) var recordingStations: FetchedResults<RecordingStation>
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(fetchRequest: RecordingStation.getRecordingStationsFetchRequest()) var recordingStations: FetchedResults<RecordingStation>
   
    /// Access watch or preparing json ?
    @State private var isJson: Bool = false
    
    var body: some View {
        
        NavigationView {
            ZStack {
                Color.primary
                    .edgesIgnoringSafeArea(.all)
                VStack {
                    /// Menu with 2 buttons
                    Spacer(minLength: 30)
                    HStack(spacing: 60) {
                        
                        PrimaryHomeView(isJson: isJson, recordingStations: recordingStations.compactMap {$0})
                        
                    }.padding(.bottom, 30)
                    
                    /// Favorite Station Title
                    TertiaryHomeView(title: "FAVORIS")
                    
                    QuaternaryHomeView(recordingStations: recordingStations)
                    
                }.navigationBarTitle("TRANSPARIS", displayMode: .large)
                .background(Color.primary) // Color behind menu button
                .setupNavBar()
                .edgesIgnoringSafeArea(.bottom)
            }
        }
    }
}



//********************************************************************************************

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().environment(\.colorScheme, .light)
    }
}

public struct SetupNavBarModifier: ViewModifier {
    public func body(content: Content) -> some View {
        content.onAppear {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            appearance.largeTitleTextAttributes = [
                .font: UIFont(name: TransparisFont.secondary.rawValue, size: 30) ?? UIFont.systemFont(ofSize: 30),
                NSAttributedString.Key.foregroundColor: UIColor.secondary
            ]
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().tintColor = .primary
            UINavigationBar.appearance().backgroundColor = .primary
            UINavigationBar.appearance().barTintColor = .primary
        }
    }
}

extension View {
    public func setupNavBar() -> some View {
        modifier(SetupNavBarModifier())
    }
}
