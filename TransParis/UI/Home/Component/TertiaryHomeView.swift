//
//  HomeFavoriteStationTitle.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI

public struct TertiaryHomeView: View {
    let title : String
    
    public var body: some View {
        VStack {
            Text(title)
                .frame(width: 100, height: 20, alignment: .center)
                .font(.caption)
                .foregroundColor(Color.primary)
                .background(Color.secondary)
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 30, alignment: .leading)
    }
}
