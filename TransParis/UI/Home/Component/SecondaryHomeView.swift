//
//  HomeCustomButtonView.swift
//  GetCity
//
//  Created by Ludovic Grimbert on 08/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct SecondaryHomeView: View {
    let imageName: String
    let title : String
    
    public var body: some View {
        ZStack {
            
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .high, vision: .shadow, width: 120, height: 120, color: .primary)

            VStack {
                Image(imageName)
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 50, height: 50)
                    .clipShape(Rectangle())
                
                Text(title)
                    .font(.caption)
                    .foregroundColor(Color.secondary)
            }
        }
    }
}
