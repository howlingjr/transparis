//
//  HomeFavoriteStationView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct QuinternaryHomeView: View {
    let station : Station
    
    public var body: some View {
        ZStack {
            LGNeumorphismView(style: .roundedRectangle(cornerRadius: 25), level: .high, vision: .shadow, width: 200, height: 60, color: Color.primary)

            
            HStack {
                
                Text("\(station.code)")
                    .frame(width: 35, height: 35, alignment: .center)
                    .foregroundColor(Color.secondary)
                    .customFont(name: TransparisFont.tertiary.rawValue, style: .body, weight: .bold)
                    .overlay(
                        Circle()
                            .stroke(Color.secondary, lineWidth: 1))
                
                Image("\(station.type)")
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 21, height: 26, alignment: .center)
                    .padding(.trailing, -5)
                    .padding(.leading, 2)
                
                Text(station.renameTypeTransport())
                    .frame(width: 40, height: 26, alignment: .center)
                    .foregroundColor(Color.primary)
                    .customFont(name: TransparisFont.tertiary.rawValue, style: .caption1, weight: .bold)
                    .background(Circle()
                        .fill(Color.secondary)).opacity(1.0)
            }
        } .frame(width: 300, height: 100,alignment: .leading)
    }
}
