//
//  PrimaryHomeView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 27/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism

public struct PrimaryHomeView: View {
    var isJson: Bool
    var recordingStations : [RecordingStation]?
   
    public var body: some View {
        Button(action: {
            print("Push addStation view")
        }) {
            NavigationLink(destination: SearchStationView()) {
                SecondaryHomeView(imageName: "search", title: "Rechercher")}
        }
        
        Button(action: {
            print("Push watch or json view")
        }) {
            if isJson {
                NavigationLink(destination: JsonStationView()) {
                    SecondaryHomeView(imageName: "cross1", title: "Json")}
            } else {
                NavigationLink(destination: WatchView(recordingStations: recordingStations)) {
                    SecondaryHomeView(imageName: "watch", title: "Apple Watch")
                }
            }
        }
    }
}
