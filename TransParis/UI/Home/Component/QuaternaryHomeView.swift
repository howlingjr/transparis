//
//  QuaternaryHomeView.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 27/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI

public struct QuaternaryHomeView: View {
    var recordingStations: FetchedResults<RecordingStation>
    
    public var body: some View {
        /// Favorites Stations
        ScrollView{
            LazyVStack {
                ForEach(self.recordingStations.compactMap {$0.allAtributes}
                            .compactMap { $0.name}
                            .removingDuplicates(), id:\.self) { name in
                    Section(header:
                                Text(name.capitalized)
                                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 100, alignment: .leading)
                                .padding(.leading, 15)
                                .foregroundColor(Color.secondary)
                                .background(Color.primary)
                                .customFont(name: TransparisFont.primary.rawValue, style: .title1, weight: .light)) {
                        ForEach(self.recordingStations.filter {$0.name == name}
                                    .compactMap {$0.allAtributes}, id: \.id) { station in
                            
                            NavigationLink(destination: ScheduleView(station: station, isEnableToDelete: true)) {
                                QuinternaryHomeView(station: station)
                            }
                        }.listRowBackground(Color.primary)
                    }.background(Color.clear)
                }
            } /// LazyVStack
        }
        .contentShape(Rectangle())
        .clipped()
        
    }
}
