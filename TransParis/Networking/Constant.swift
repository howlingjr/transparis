//
//  Constants.swift
//  GetCity
//
//  Created by Ludovic Grimbert on 08/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

enum BuildConfigurationType: Int {
    case UNKNOWN = 0
    case DEBUG
    case RELEASE
}

struct Constant {

    /// Constants used to modify the app's workflow (enable and disable functionalities)
    struct BuildConfiguration {

        // Tell if the build sheme is production, release or testflight configuration
        static var currentBuildConfiguration: BuildConfigurationType {
            #if DEBUG
            return BuildConfigurationType.DEBUG
            #elseif RELEASE
            return BuildConfigurationType.RELEASE
            #else
            return BuildConfigurationType.UNKNOWN
            #endif
        }
    }

    /// Constants used to determine the domain to target
    struct Domain {

        // The development domain
        static let Developement: String = "https://api-ratp.pierre-grimaud.fr/v4/"

        // The production domain
        static let Production: String = "https://api-ratp.pierre-grimaud.fr/v4/"

        static var CurrentBaseURL: String {

            // Exception case for internal testFlight builds
            switch BuildConfiguration.currentBuildConfiguration {
            case .DEBUG :
                return Domain.Developement
            case .RELEASE :
                return Domain.Production
            case .UNKNOWN :
                return Domain.Developement
            }

        }
    }
}
