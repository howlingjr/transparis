//
//  APIRouter.swift
//  GetCity
//
//  Created by Ludovic Grimbert on 08/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public enum APIRouter {
    
    case schedules(AR: String, stationSlug: String,code: String, type: String, baseURL: String)
    case lines(type: String, baseURL: String)
    case stations(type: String, code: String, baseURL: String )
    
    // MARK: - Base URL
    public var baseURL: String {
        switch self {
        case .schedules(_,_,_,_, let baseURL):
            return baseURL
        case .lines(type: _, let baseURL):
            return baseURL
        case .stations(type: _, code: _, let baseURL):
            return baseURL
        }
    }
    
    // MARK: - Path
    public var path: String {
        switch self {
        case .schedules(let AR,let stationSlug,let code,let type, _):
            return baseURL + "schedules/\(type)/\(code)/\(stationSlug)/\(AR)/?"
        case .lines(let type, _):
            return baseURL + "lines/\(type)/?"
        case .stations(let type, let code, _):
            return baseURL + "stations/\(type)/\(code)/?"
        }
    }
    
}
