//
//  Way.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public enum Way : String {
    case directionA = "A"
    case directionR = "R"
    case none = ""
}
