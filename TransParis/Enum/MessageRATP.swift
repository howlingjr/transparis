//
//  MessageRATP.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public enum UnusedMessageRATP: String, CaseIterable {
    case trainRetarde = "train retarde"
    case trainRetardé = "train retardé"
    case unavailable = "unavailable"
    case serviceTermine = "service termine"
    case serviceTermine2 = "service terminé"
    case points = "........."
    case traficReduit = "trafic reduit"
    case info = "info"
    case dernierPassage = "dernier passage"
    case arretNonDesservi = "arret non desservi"
    case manifestation = "manifestation"
    case negative = "-"
    case premierPassage = "premier passage"
    case trainArrete = "train arrete"
    case sansvoyageurs = "sans voyageurs"
    case service = "service"
    case noncommence = "non commence"
    case pasdeservice = "pas de service"
    case sansArret = "train sans arrêt"

//    case stationne = "stationne"
//    case perturbations = "perturbations"
    
    static var messagesList: [String] {
        return UnusedMessageRATP.allCases.map { $0.rawValue }
    }
}

public enum GoodMessageRATP: String, CaseIterable {
    /// Important order
    case trainAQuai = "train a quai"
    case trainAQuaiFix = "train à quai"
    case aLarret = "a l'arret"
    case aLarretFix = "à l'arret"
    case trainApproche = "train a l'approche"
    case trainApprocheFix = "train à l'approche"
    case approche = "a l'approche"
    case approcheFix = "à l'approche"
    case zeroMinute = "0 mn"
    case oneMinute = "1 mn"
   
    static var messagesList: [String] {
        return GoodMessageRATP.allCases.map { $0.rawValue }
    }
}
