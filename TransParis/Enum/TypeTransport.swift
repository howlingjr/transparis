//
//  TypeTransport.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public enum TypeTransport : String, CaseIterable {
    case metros
    case rers
    case buses
    case tramways
    case noctiliens
    
    var description: String {
        get {
            switch self {
            case .metros:
                return "M"
            case .rers:
                return "Rer"
            case .buses:
                return "Bus"
            case .tramways:
                return "T"
            case .noctiliens:
                return "N"
            }
        }
    }
    
    static var cases: [TypeTransport] {
           return TypeTransport.allCases.map { $0 }
       }
    
    static var list: [String] {
           return TypeTransport.allCases.map { $0.rawValue }
       }
    
    static var shortList: [String] {
        return TypeTransport.allCases.map { $0.description }
    }
    
}
