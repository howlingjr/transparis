//
//  Station.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import CoreData

//public class RecordingStation: NSManagedObject {
//
//    @NSManaged public var id: String
//    @NSManaged public var name: String
//    @NSManaged public var type: String
//    @NSManaged public var code: String
//    @NSManaged public var imageName: String
//    @NSManaged public var slug: String
//
//    var allAtributes : Station {
//        get {
//            return Station(id: self.id,
//                           name: self.name,
//                           type: self.type,
//                           code: self.code,
//                           imageName: self.imageName,
//                           slug: self.slug)
//        }
//        set {
//            self.id = newValue.id
//            self.name = newValue.name
//            self.type = newValue.type
//            self.code = newValue.code
//            self.imageName = newValue.imageName
//            self.slug = newValue.slug
//        }
//    }
//}

public struct StationRawResponse: Codable {
    let result: StationResultResponse
}

public struct StationResultResponse: Codable {
    let stations: [Station]
}

struct Station: Codable, Identifiable, Equatable, Hashable {
    let id: String
    let name: String
    let type: String
    let code: String
    let imageName: String
    let slug: String
    
    static func == (lhs: Station, rhs: Station) -> Bool {
        return lhs.name == rhs.name && lhs.type == rhs.type && lhs.code == rhs.code // remove duplicates from JSON
      //return lhs.name == rhs.name
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
    
    func renameTypeTransport() -> String {
        switch self.type {
        case "metros" : return TypeTransport.metros.description
        case "rers" : return TypeTransport.rers.description
        case "buses" : return TypeTransport.buses.description
        case "tramways" : return TypeTransport.tramways.description
        case "noctiliens" : return TypeTransport.noctiliens.description
        default:
            return ""
        }
    }
}

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
