//
//  LineRawResponse.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 23/06/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public struct MetroRawResponse: Codable {
    let result: MetroResultResponse
}
public struct MetroResultResponse: Codable {
    let metros: Lines
}

public struct BusRawResponse: Codable {
    let result: BusResultResponse
}
public struct BusResultResponse: Codable {
    let buses: Lines
}

public struct NoctilienRawResponse: Codable {
    let result: NoctilienResultResponse
}
public struct NoctilienResultResponse: Codable {
    let noctiliens: Lines
}

public struct RerRawResponse: Codable {
    let result: RerResultResponse
}
public struct RerResultResponse: Codable {
    let rers: Lines
}

public struct TramwayRawResponse: Codable {
    let result: TramwayResultResponse
}
public struct TramwayResultResponse: Codable {
    let tramways: Lines
}

public struct Line: Codable, Identifiable {
    public let id :String
    let code: String
    let name: String
    let directions: String
}

public typealias Lines = [Line]

public struct RawStationRawResponse: Codable {
    let result: RawStationResultResponse
}

public struct RawStationResultResponse: Codable {
    let stations: [RawStation]
}

public struct RawStation: Codable {
    let name: String
    let slug: String
}
