//
//  RecordingStation.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 02/01/2021.
//  Copyright © 2021 ludovic grimbert. All rights reserved.
//

import Foundation
import CoreData

public class RecordingStation: NSManagedObject {
    
    @NSManaged public var id: String
    @NSManaged public var name: String
    @NSManaged public var type: String
    @NSManaged public var code: String
    @NSManaged public var imageName: String
    @NSManaged public var slug: String

    var allAtributes : Station {
        get {
            return Station(id: self.id,
                           name: self.name,
                           type: self.type,
                           code: self.code,
                           imageName: self.imageName,
                           slug: self.slug)
        }
        set {
            self.id = newValue.id
            self.name = newValue.name
            self.type = newValue.type
            self.code = newValue.code
            self.imageName = newValue.imageName
            self.slug = newValue.slug
        }
    }
}

extension RecordingStation {
    static func getRecordingStationsFetchRequest() -> NSFetchRequest<RecordingStation>{
        let request = RecordingStation.fetchRequest() as! NSFetchRequest<RecordingStation>
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        return request
    }
}
