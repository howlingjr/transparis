//
//  RawReponse.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public struct RawResponse: Codable {
    let result: ResultResponse
}

public struct ResultResponse: Codable {
    let schedules: [Schedule]
}

public struct Schedule: Codable {
 //   public let id = UUID()
    let code: String?
    let message: String
    let destination: String
    
    enum CodingKeys: String, CodingKey {
        case code
        case message
        case destination
      }
}
extension Schedule: Identifiable  {
    public var id: UUID { return UUID() }
}
public typealias Schedules = [Schedule]
