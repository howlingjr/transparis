//
//  ScheduleGroupingVo.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public struct ScheduleGroupingVo: Identifiable,Hashable {
    public var id: UUID
    var destination: String
    var way: Way
    var result: [ScheduleVo]
    var isClosed: Bool
}
