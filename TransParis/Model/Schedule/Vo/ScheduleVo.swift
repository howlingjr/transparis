//
//  ScheduleVo.swift
//  TransParis
//
//  Created by Ludovic Grimbert on 28/05/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation

public struct ScheduleVo: Identifiable,Hashable {
    public var id: UUID
    var message: String
    var destination: String
    var way: Way
    var code: String
    
    init(id: UUID, message: String, destination: String,code: String,way: Way) {
        self.id   = id
        self.message = message
        self.destination  = destination
        self.way = way
        self.code = code
    }
    init() {
        self.id = UUID()
        self.message = ""
        self.destination = ""
        self.way = .directionA
        self.code = ""
    }
}
