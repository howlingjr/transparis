//
//  WatchConnectivity.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 28/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import WatchConnectivity

class WatchConnectivity : NSObject,  WCSessionDelegate, ObservableObject{
    var session: WCSession

    init(session: WCSession = .default ){
        self.session = session
        super.init()
        self.session.delegate = self
        session.activate()
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            let jsonString  = message["message"] as? String ?? "Unknown"
            let jsonData = jsonString.data(using: .utf8)!
            let stationRaw = try! JSONDecoder().decode(StationRawResponse.self, from: jsonData)
            print(stationRaw.result.stations)
        }
    }
}
