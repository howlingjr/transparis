//
//  SecondaryHomeWatchView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 29/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import CustomFont

public struct SecondaryHomeWatchView: View {
    let station : Station
    
    public var body: some View {
        ZStack {

            HStack {
                
                Text("\(station.code)")
                    .frame(width: 40, height: 30, alignment: .trailing)
                    .foregroundColor(Color.primary)
                    .customFont(name: TransparisFont.tertiary.rawValue, style: .title1, weight: .bold)

                Image("\(station.type)")
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 21, height: 26, alignment: .center)

                Text(station.renameTypeTransport())
                    .frame(width: 50, height: 30, alignment: .leading)
                    .foregroundColor(Color.primary)
                    .customFont(name: TransparisFont.tertiary.rawValue, style: .title1, weight: .bold)

            }
        }
    }
}
