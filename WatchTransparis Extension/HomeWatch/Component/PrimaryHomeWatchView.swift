//
//  HomeWatchPrimaryView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 29/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism
import CustomFont

public struct PrimaryHomeWatchView: View {
    var recordingStations: FetchedResults<RecordingStation>
    public var body: some View {
        ZStack {
            ScrollView{
                LazyVStack {
                    ForEach(self.recordingStations.compactMap {$0.allAtributes}
                                .compactMap { $0.name}
                                .removingDuplicates(), id:\.self) { name in
                        Section(header:
                                    Text(name.capitalized)
                                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                                    .foregroundColor(Color.primary)
                                    .customFont(name: TransparisFont.primary.rawValue, style: .title3, weight: .light)) {
                            ForEach(self.recordingStations.filter {$0.name == name}
                                        .compactMap {$0.allAtributes}, id: \.id) { station in
                                NavigationLink(destination: ScheduleWatchView(station: station)) {SecondaryHomeWatchView(station: station) }
                            }
                                    }
                                }
                }
            }
        }
    }
}
