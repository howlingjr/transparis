//
//  ContentView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 24/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import SwiftUI
import CoreData

struct HomeWatchView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(fetchRequest: RecordingStation.getRecordingStationsFetchRequest()) var recordingStations: FetchedResults<RecordingStation>
    
    var body: some View {
        PrimaryHomeWatchView(recordingStations: self.recordingStations)
    }
}

struct HomeWatchView_Previews: PreviewProvider {
    static var previews: some View {
        HomeWatchView()
    }
    
}
