//
//  ScheduleWatchView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 29/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI

public struct ScheduleWatchView: View {
    
    @State var currentTimePublisher = Timer.TimerPublisher(interval: 30.0, runLoop: .main, mode: .common).autoconnect() // 30 secondes
    let station: Station
    @ObservedObject var viewModel: ScheduleViewModel = ScheduleViewModel()
    
    public var body: some View {
        GeometryReader { geometry in
            VStack {
                
                PrimaryScheduleWatchView(station: station, viewModel: self.viewModel, action: { self.viewModel.requestSchedules(station: self.station) })
//                ScheduleCustomEmptyView(isDisplaying: self.viewModel.customEmptyViewIsDisplaying) //TODO ?
                SecondaryScheduleWatchView(schedulesGroupingVo: self.viewModel.scheduleGroupingVo)
                
                
            }  /// Fetch data + Timer
            .onAppear {
                self.instantiateTimer()
                /// Launch initial request
                self.viewModel.requestSchedules(station: self.station)
            }.onDisappear {
                /// Delete Timer
                self.currentTimePublisher.upstream.connect().cancel()
            }.onReceive(currentTimePublisher) { newCurrentTime in
                print(newCurrentTime)
                /// Launch request after x seconds
                self.viewModel.requestSchedules(station: self.station)
            }
        }
    }
    
    func instantiateTimer() {
        print("INTERVAL 30 SECONDES ⏰")
        self.currentTimePublisher = Timer.TimerPublisher(interval: 30.0, runLoop: .main, mode: .common).autoconnect()
    }
    
    
}
