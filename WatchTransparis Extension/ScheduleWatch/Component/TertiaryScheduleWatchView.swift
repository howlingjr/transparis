//
//  TertiaryScheduleWatchView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 05/01/2021.
//  Copyright © 2021 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGNeumorphism
import LGActivityIndicator


public struct TertiaryScheduleWatchView: View {
    let scheduleGroupingVo: ScheduleGroupingVo
    @State var loading = true
    
    public var body: some View {
        
        VStack {
            
            Text(scheduleGroupingVo.destination)
                .frame(maxWidth: .infinity, alignment: .leading)
                .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .light)
                .foregroundColor(Color.primary)
            
            HStack {
               // if scheduleGroupingVo.isClosed {
                    ZStack{
                        Circle()
                            .frame(width: 5, height: 5, alignment: .center)
                            .padding(.trailing, 10)
                            .foregroundColor(scheduleGroupingVo.isClosed ? Color.tertiary : Color.primary)
                         if scheduleGroupingVo.isClosed {
                    LGActivityIndicatorView(isVisible: $loading, type: .circleDots(count: 5))
                        .frame(width: 19, height: 19)
                        .padding(.trailing, 10)
                        .foregroundColor(Color.tertiary)
                         }
                }.frame(width: 25, height: 25)
          //  }
                
            Text("\(scheduleGroupingVo.result.first!.message)")
               // .frame( alignment: .leading)
                .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .bold)
                .foregroundColor(Color.secondary)
            
            Text("\(scheduleGroupingVo.result.first!.code)")
               // .frame( alignment: .leading)
                .customFont(name: TransparisFont.primary.rawValue, style: .body, weight: .bold)
                .foregroundColor(Color.secondary)
        
                
                
            }.frame(maxWidth: .infinity, alignment: .leading)

        Spacer(minLength: 20)
    }
}
}
