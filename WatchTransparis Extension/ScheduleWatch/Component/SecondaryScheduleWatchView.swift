//
//  SecondaryScheduleWatchView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 04/01/2021.
//  Copyright © 2021 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI

public struct SecondaryScheduleWatchView: View {
    let schedulesGroupingVo: [ScheduleGroupingVo]
    
    public var body: some View {
        scrollForEach
    }
    
    var scrollForEach: some View {
        ScrollView {
            LazyVStack {
                ForEach(schedulesGroupingVo) { scheduleGroupingVo in
                    TertiaryScheduleWatchView(scheduleGroupingVo: scheduleGroupingVo)
                }
            }
        }
    }
}
