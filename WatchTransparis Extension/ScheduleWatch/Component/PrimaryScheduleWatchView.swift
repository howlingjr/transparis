//
//  PrimaryScheduleWatchView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 03/01/2021.
//  Copyright © 2021 ludovic grimbert. All rights reserved.
//

import Foundation
import SwiftUI
import LGActivityIndicator


public struct PrimaryScheduleWatchView: View {
    let station : Station
    @ObservedObject var viewModel: ScheduleViewModel
    var action: () -> Void
    
    public var body: some View {
        Spacer(minLength: 3)
        VStack {
            HStack(spacing: 5) {
                VStack {
                    Text(station.renameTypeTransport() + " " + station.code)
                        .frame(minWidth: 0, maxWidth: 60, minHeight: 0, maxHeight: 30, alignment: .center)
                        .foregroundColor(Color.primary)
                        .customFont(name: TransparisFont.tertiary.rawValue, style: .body, weight: .bold)
                    
                    Button(action: {
                        print("Call schedules by push button ")
                        self.action()
                    }) {
                        ZStack {
                            HStack(spacing: 3) {
                                Circle().frame(width: 8, height: 8, alignment: .center).foregroundColor(Color.black)
                                Circle().frame(width: 8, height: 8, alignment: .center).foregroundColor(Color.black)
                                Circle().frame(width: 8, height: 8, alignment: .center).foregroundColor(Color.black)
                            }
                            
                            LGActivityIndicatorView(isVisible: $viewModel.loading, type: .ghostDots(count: 3))
                                .frame(width: 30, height: 10,alignment: .trailing)
                                .foregroundColor(Color.tertiary)
                        }
                    }.overlay(
                        RoundedRectangle(cornerRadius: 15)
                            .stroke(Color.tertiary, lineWidth: 3))
                    
                }
                Text(station.name.capitalized)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 60, maxHeight: 100, alignment: .center)
                    .foregroundColor(Color.primary)
                    .customFont(name: TransparisFont.tertiary.rawValue, style: .title3, weight: .regular)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 15)
                    .stroke(Color.tertiary, lineWidth: 3))
            
        }
    }
}
