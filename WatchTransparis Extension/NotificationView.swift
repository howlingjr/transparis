//
//  NotificationView.swift
//  WatchTransparis Extension
//
//  Created by Ludovic Grimbert on 24/12/2020.
//  Copyright © 2020 ludovic grimbert. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
